<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('student.index'));
});

Route::get('/students', 'StudentController@index')->name('student.index');
Route::get('/students/groups', 'GroupController@index')->name('group.index');
Route::get('/students/subjects', 'SubjectController@index')->name('subject.index');
Route::get('/students/update/{id}', 'StudentController@edit')->name('student.edit');

Route::post('/students/create', 'StudentController@create')->name('student.create');
Route::post('/students/group/create', 'GroupController@create')->name('group.create');
Route::post('/students/subject/create', 'SubjectController@create')->name('subject.create');
Route::post('/students/mark/update/', 'MarkController@update')->name('mark.update');

Route::post('/students/update/set', 'StudentController@update')->name('student.update');
Route::post('/students', 'StudentController@index')->name('student.search');

Route::post('/students/group/update', 'GroupController@update')->name('group.update');
Route::post('/students/subject/update', 'SubjectController@update')->name('subject.update');

Route::delete('/students/delete/{student}', 'StudentController@delete')->name('student.delete');
Route::delete('/students/group/delete/{group}', 'GroupController@delete')->name('group.delete');
Route::delete('/students/subject/delete/{subject}', 'SubjectController@delete')->name('subject.delete');
Route::delete('/students/mark/delete/{mark}', 'MarkController@delete')->name('mark.delete');
