<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Student;
use App\Subject;

class Mark extends Model
{
    protected $fillable = [
        'student_id',
        'subject_id',
        'mark'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
