<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name',
        'born',
        'group_id',
    ];

    private static $search_items = [
        'name'     => 'Name',
        'group'    => 'Group',
        'born'     => 'Birthday',
        'marks'    => 'Mark',
        'avg_mark' => 'Avg mark'
    ];

    private static $search_models = [
        'name' => null,
        'born' => null,
        'group' => [
            'col'   => 'name',
            'by'    => 'id',
            'in'    => 'group_id',
            'model' => Group::class,
        ],
        'marks' => [
            'in'  => 'id',
            'col' => 'mark',
            'by'  => 'student_id',
            'model' => Mark::class
        ],
        'avg_mark' => [
            'in'  => 'id',
            'col' => 'avg_mark',
            'by'  => 'student_id',
            'model' => Avg::class
        ]
    ];

    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id', 'id');
    }

    public function marks()
    {
        return $this->hasMany(Mark::class, 'student_id', 'id');
    }

    public function avg()
    {
        return $this->belongsTo(Avg::class, 'id', 'student_id');
    }

    public static function getSearchItems($key = null)
    {
        if ($key) {
            return self::$search_items[$key];
        }

        return self::$search_items;
    }

    public static function getSearchModels($key = null)
    {
        if ($key) {
            return self::$search_models[$key];
        }

        return self::$search_models;
    }


}
