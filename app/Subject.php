<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Mark;

class Subject extends Model
{
    protected $fillable = [
        'name'
    ];

    public function subjects()
    {
        return $this->hasMany(Mark::class);
    }
}
