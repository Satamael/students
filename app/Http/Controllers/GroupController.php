<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
use App\Group;
use App\Mark;
use App\Avg;

class GroupController extends Controller
{
    public function index()
    {
        $groups = Group::all();

        return view('group.index', ['groups' => $groups]);
    }

    public function delete(Group $group)
    {
        $students = Student::where('group_id', $group->id)->get();

        foreach ($students as $student) {
            $list_students_id[] = $student->id;
        }

        Mark::whereIn('student_id', $list_students_id)->delete();

        Avg::whereIn('student_id', $list_students_id)->delete();

        Student::where('group_id', $group->id)->delete();

        $group->delete();

        return back();
    }

    public function create(Request $request)
    {
        if ($request->name) {
            Group::create(['name' => $request->name]);
        }

        return back();
    }

    public function update(Request $request)
    {
        if ($request->id) {
            Group::where('id', $request->id)
                ->update(['name' => $request->name]);
        }

        return redirect(route('group.update'));
    }
}
