<?php

namespace App\Http\Controllers;

use App\Avg;
use App\Mark;
use Illuminate\Http\Request;
use App\Student;
use App\Group;
use App\Subject;
use stdClass;

class StudentController extends Controller
{
    public function index(Request $request)
    {
        $groups      = Group::all();
        $subjects    = Subject::all();
        $students    = Student::with('group', 'avg', 'marks.subject');
        $search_list = new stdClass();

        $search_list->search_groups = Student::getSearchItems();

        if ($request->search) {
            if (Student::getSearchModels($request->search_item)) {
                $search_model = Student::getSearchModels($request->search_item);

                $students = $this->search($request->search, $search_model['model'], $search_model['col'], $search_model['by'], $search_model['in']);
            } else {
                $students = $students->where($request->search_item, 'LIKE', '%' . $request->search . '%')->get();
            }

            $search_list->search      = isset($request->search) ? $request->search : null;
            $search_list->search_item = Student::getSearchItems($request->search_item);

        } else {
            $students                 = $students->get();
            $search_list->search      = null;
            $search_list->search_item = null;
        }

        return view("student.index" , [
            'students'     => $students,
            'groups'       => $groups,
            'subjects'     => $subjects,
            'search_list'  => $search_list
        ]);
    }

    public function search($search_string, $model, $col, $by, $in) {

            $searched_items = $model::all()->first()->where($col, 'LIKE' , '%' . $search_string . '%')->get();

            $search_array = [];

            foreach ($searched_items as $items) {
                $search_array[] = $items->{$by};
            }

            return $students = Student::with('group', 'avg', 'marks.subject')->whereIn($in, $search_array)->get();
    }

    public function delete(Student $student)
    {
        Mark::where('student_id', $student->id)->delete();

        Avg::where('student_id', $student->id)->delete();

        $student->delete();

        return redirect(route('student.index'));
    }

    public function create(Request $request)
    {
        if ($request->name && $request->born && $request->group_id) {
            Student::create([
                'name'     => $request->name,
                'born'     => $request->born,
                'group_id' => $request->group_id
            ]);
        }

        return back();
    }

    public function edit(Request $request)
    {
        $student = Student::with('group', 'avg', 'marks.subject');
        $groups = Group::all();
        $subjects = Subject::all();

        if ($request->id) {
            $student = $student->where('id', $request->id)->get();
        }

        return view("student.edit" , [
            'student'  => $student->first(),
            'subjects' => $subjects,
            'groups'   => $groups
        ]);
    }

    public function update(Request $request)
    {
        if ($request->id) {
            Student::where('id', $request->id)
                ->update(['name' => $request->name,
                          'born' => $request->born]);
        }

        if ($request->is_group) {
            Student::where('id', $request->id)
                ->update(['group_id' => $request->group_id]);
        }

        return back()->withInput();
    }
}
