<?php

namespace App\Http\Controllers;

use App\Mark;
use App\Avg;
use App\Student;
use Illuminate\Http\Request;

class MarkController extends Controller
{
    public function update(Request $request)
    {
        if ($request->mark != '') {
            Mark::updateOrCreate(
                ['id' => $request->id],
                ['mark' => $request->mark,
                    'student_id' => $request->student_id,
                    'subject_id' => $request->subject_id
                ]);

            self::avg($request->student_id);
        }
        return back()->withInput();
    }

    public function delete(Mark $mark)
    {
        $mark->delete();

        self::avg($mark->student_id);

        return back()->withInput();
    }

    public static function avg($id = null)
    {
        if (!$id) {
            $students = Student::all();
        } else {
            $students = Student::where('id', $id)->get();
        }

        if (!$students) {
            return null;
        }

        foreach ($students as $student) {
            $marks_sum = $student->marks->sum('mark');
            $marks_int = count($student->marks);

            if (!$marks_int) {
                Avg::where('student_id', $student->id)->delete();

            } else {
                $avg_mark = round((float) $marks_sum / (float) $marks_int, 1);

                if ($avg_mark < 2) {
                    $signal = 'danger';
                } elseif ($avg_mark >= 2 && $avg_mark < 4.5) {
                    $signal = 'info';
                } elseif ($avg_mark >= 4.5) {
                    $signal = 'success';
                }

                Avg::updateOrCreate(
                    ['student_id' => $student->id],
                    ['avg_mark' => $avg_mark, 'signal' => $signal]
                );
            }
        }
    }
}
