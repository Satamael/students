<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Mark;

class SubjectController extends Controller
{
    public function index()
    {
    	$subjects = Subject::all();

    	return view('subject.index',
            ['subjects' => $subjects]);
    }

    public function delete(Subject $subject)
    {
        Mark::where('subject_id', $subject->id)->delete();

        $subject->delete();

        MarkController::avg();

        return back();
    }

    public function create(Request $request)
    {
        if ($request->name) {
            Subject::create(['name' => $request->name]);
        }

        return back();
    }

    public function update(Request $request)
    {
        if ($request->id) {
            Subject::where('id', $request->id)
                ->update(['name' => $request->name]);
        }

        return redirect(route('student.index'));
    }
}
