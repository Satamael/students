<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avg extends Model
{
    protected $fillable = [
        'student_id',
        'avg_mark',
        'id',
        'signal'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
