#Students



**In the "Students" directory:**

- `composer update`
- **You must create database and ".env" file like ".env.example":**

        DB_DATABASE=your_database_name
        DB_USERNAME=your_user_name
        DB_PASSWORD=your_password
        
- `php artisan migrate`
- `php artisan serve`
