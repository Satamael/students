<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Students</title>
</head>
<body>
<main>
    <div class="container">
        <div class="col">
            <div class="row">
                <div class="col page-title">
                    <h3>Groups</h3>
                </div>
            </div>
            <div class="row p-3 card">
                <form action="{{ route('create_group') }}" method="POST">
                    @csrf
                    <div class="form-group card-body">
                        <p class="card-text">New Group</p>
                        <input type="text" class="form-control" name="name" placeholder="Enter Name">
                    </div>
                    <button type="submit" class="btn btn-primary ml-3">Create</button>
                    <a class="btn btn-warning ml-3" href="{{ route('index') }}">Exit</a>
                </form>
            </div>
            <div class="row mt-3">
                <table class="table table-striped table-dark">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($groups as $item)
                        <tr>
                            <th scope="row">{{ $loop->iteration + 1 }}</th>
                            <td>
                                <form action="{{ route('group_update')}}" method="POST">
                                    @csrf
                                    <input onchange="" type="text" class="form-control" name="name" value="{{ $item->name }}">
                                    <input type="hidden" class="form-control" name="id" value="{{ $item->id }}">
                                </form>
                            </td>
                            <td>
                                <form action="{{ route('delete_group', ['id' => $item->id]) }}" method="POST">
                                    @csrf
                                    {{ method_field("DELETE") }}
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
</body>
</html>