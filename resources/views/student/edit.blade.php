<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Students</title>
</head>
<body>
<main>
    <div class="container">
        <div class="col">
            <div class="row card-group mt-3">
                <div class="card col-4 p-0 text-center">
                    <img src="https://memepedia.ru/wp-content/uploads/2016/03/hide-the-pain-harold.jpg" class="card-img-top" alt="photo">
                    <div class="card-body">
                        <h5 class="card-title"><strong>{{ $student->name }}</strong></h5>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <h5 class="card-title">
                                Group: <span class="badge badge-info">{{ $student->group->name }}</span>
                            </h5>
                        </li>
                        <li class="list-group-item">
                            <h5 class="card-title">
                                Average mark: <span class="badge badge-{{ $student->avg->signal ?? 'light' }}">
                                    {{ $student->avg->avg_mark ?? 'None' }}
                                </span>
                            </h5>
                        </li>
                    </ul>
                    <div class="card-body">
                        <form action="{{ route('student.delete', ['id' => $student->id]) }}" method="POST">
                            @csrf
                            {{ method_field("DELETE") }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                            <a class="btn btn-warning ml-3" href="{{ route('student.index') }}">Exit</a>
                        </form>
                    </div>
                </div>
                <div class="col-8 card p-3">
                    <form action="{{ route('student.update') }}" method="POST">
                        @csrf
                        <div class="form-group card-body">
                            <p class="card-text"><input type="checkbox" name="is_group" value="true"> Change Group:</p>
                            <select class="form-control" name="group_id">
                                @foreach($groups as $group)
                                    <option value="{{ $group->id }}">
                                        {{ $group->name }}
                                    </option>
                                @endforeach
                            </select>
                            <br>
                            <p class="card-text">Name:</p>
                            <input type="text" class="form-control" name="name" value="{{ $student->name }}">
                            <input type="hidden" class="form-control" name="id" value="{{ $student->id }}">
                            <br>
                            <p class="card-text">Birthday:</p>
                            <input type="text" class="form-control" name="born" value="{{ $student->born }}">
                        </div>
                        <button type="submit" class="btn btn-primary ml-3">Update</button>
                    </form>
                </div>
            </div>
            <div class="row mt-3">
                <table class="table table-striped table-dark">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Subject Name</th>
                        <th scope="col">Mark</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($subjects as $subject)
                        <tr>
                            <th scope="row">{{ $loop->index + 1 }}</th>
                            <th scope="row">{{ $subject->name }}</th>
                            <td>
                                <form action="{{ route('mark.update')}}" method="POST">
                                    @csrf
                                    <input onchange="" type="text" class="form-control col-1" name="mark" value="{{ $student->marks->where('subject_id', $subject->id)->first()->mark ?? null }}">
                                    <input type="hidden" class="form-control" name="id" value="{{ $student->marks->where('subject_id', $subject->id)->first()->id ?? null}}">
                                    <input type="hidden" class="form-control" name="student_id" value="{{ $student->id }}">
                                    <input type="hidden" class="form-control" name="subject_id" value="{{ $subject->id}}">
                                </form>
                            </td>
                            <td>
                                <form action="{{ route('mark.delete', ['id' => $student->marks->where('subject_id', $subject->id)->first()->id ?? null]) }}" method="POST">
                                    @csrf
                                    {{ method_field("DELETE") }}
                                    <button type="submit" {{ $student->marks->where('subject_id', $subject->id)->first()->id ?? 'disabled' }} class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
</body>
</html>
