<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Students</title>
</head>
<body>
    <main>
        <div class="container">
            <div class="col">
                <div class="row">
                    <div class="col page-title">
                        <h3>Students</h3>
                    </div>
                </div>
                <div class="row card-group">
                    <div class="col-9 p-3 card">
                        <form action="{{ route('student.create') }}" method="POST">
                            @csrf
                            <div class="form-group card-body">
                                <p class="card-text">New Student</p>
                                <select class="form-control col" name="group_id">
                                    @foreach($groups as $group)
                                        <option value="{{ $group->id }}">
                                            {{ $group->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <br>
                                <input type="text" class="form-control" name="name" placeholder="Enter Name">
                                <br>
                                <input type="text" class="form-control" name="born" placeholder="Birthday: YYYY-MM-DD">
                            </div>
                            <button type="submit" class="btn btn-primary ml-3">Create</button>
                        </form>
                    </div>
                    <div class="col-3 p-0 card text-center">
                        <div class="list-group list-group-flush">
                            <a class="list-group-item list-group-item-action" href="{{ route('group.index') }}">Groups</a>
                            <a class="list-group-item list-group-item-action" href="{{ route('subject.index') }}">Subjects</a>
                        </div>
                    </div>
                </div>
{{--                {{ dd($search_list) }}--}}
                <div class="row p-3 mt-3 card">
                    <form class="row" action="{{ route('student.search') }}" method="POST">
                        @csrf
                        <p class="col">Search:
                            <span class="badge badge-primary">{{ $search_list->search_item }}</span>
                            <span class="badge badge-success">{{ $search_list->search }}</span>
                        </p>
                        <select class="form-control col-2" name="search_item">
                            @foreach($search_list->search_groups as $key => $value)
                                <option value="{{ $key }}">
                                    {{ $value }}
                                </option>
                            @endforeach
                        </select>
                        <input type="text" class="form-control ml-3 col-3" name="search" placeholder="String">
                        <button type="submit" class="btn btn-primary ml-3 mr-3">
                            @if ($search_list->search)
                                Reset
                            @else
                                Start
                            @endif
                        </button>
                    </form>
                </div>
                <div class="row mt-3">
                    <table class="table table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Group</th>
                                <th scope="col">Name</th>
                                <th scope="col">Birthday</th>
                                @foreach($subjects as $subject)
                                    <th scope="col">
                                        {{ $subject->name }}
                                    </th>
                                @endforeach
                                <th scope="col">Avg mark</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($students as $item)
                            <tr>
                                <th scope="row">{{ $loop->index + 1 }}</th>
                                <td>{{ $item->group->name }}</td>
                                <td>
                                    <a class="btn p-0" style="color: yellow" href="{{ route('student.edit', ['id' => $item->id]) }}">{{ $item->name }}</a>
                                </td>
                                <td>{{ $item->born }}</td>
                                @foreach($subjects as $subject)
                                        <td>
                                            {{ $item->marks->where('subject_id', $subject->id)->first()->mark ?? 'None' }}
                                        </td>
                                @endforeach
                                <td>
                                    <span class="badge badge-{{ $item->avg->signal ?? 'light' }}">
                                        {{ $item->avg->avg_mark ?? 'None' }}
                                    </span>
                                </td>
                                <td>
                                    <form action="{{ route('student.delete', ['id' => $item->id]) }}" method="POST">
                                        @csrf
                                        {{ method_field("DELETE") }}
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
</body>
</html>
